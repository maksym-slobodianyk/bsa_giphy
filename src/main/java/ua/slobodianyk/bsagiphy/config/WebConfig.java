package ua.slobodianyk.bsagiphy.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ua.slobodianyk.bsagiphy.interceptors.LoggerInterceptor;
import ua.slobodianyk.bsagiphy.interceptors.RequestHeaderInterceptor;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(new RequestHeaderInterceptor()).addPathPatterns("/user/**", "/gifs/**", "/cache/**");
        registry.addInterceptor(new LoggerInterceptor()).addPathPatterns("/user/**", "/gifs/**", "/cache/**");
    }
}
