package ua.slobodianyk.bsagiphy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;

@Configuration
@EnableSwagger2
public class SpringFoxConfig {


    @Bean
    public Docket apiDocket() {

        ParameterBuilder aParameterBuilder = new ParameterBuilder();


        return new Docket(DocumentationType.SWAGGER_2)
                .produces(Collections.singleton("application/json"))
                .consumes(Collections.singleton("application/json"))
                .select()
                .apis(RequestHandlerSelectors.basePackage("ua.slobodianyk.bsagiphy.controller"))
                .paths(PathSelectors.any())
                .build()
                .enable(true)
                .globalOperationParameters(
                        Arrays.asList(aParameterBuilder.name("X-BSA-GIPHY")
                                .modelRef(new ModelRef("string"))
                                .parameterType("header")
                                .defaultValue("bsa20")
                                .build()))
                .apiInfo(getApiInfo());
    }


    private ApiInfo getApiInfo() {
        return new ApiInfo(
                "BSA GIPHY APP",
                "Application for generating and storing GIFs",
                "1.0",
                "Free to use",
                new Contact("Slobodianyk Maksym", "URL", "maxslobodianyk@gmail.com"),
                "API LICENSE",
                "LICENSE URL",
                Collections.emptyList()
        );

    }

}
