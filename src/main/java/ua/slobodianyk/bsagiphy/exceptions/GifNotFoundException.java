package ua.slobodianyk.bsagiphy.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "No gif was found by query")
public class GifNotFoundException extends RuntimeException {

    public GifNotFoundException() {
    }

    public GifNotFoundException(String message) {
        super(message);
    }

}
