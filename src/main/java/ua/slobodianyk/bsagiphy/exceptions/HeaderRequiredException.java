package ua.slobodianyk.bsagiphy.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "X-BSA-GIPHY header required")
public class HeaderRequiredException extends RuntimeException {

    public HeaderRequiredException() {
    }

    public HeaderRequiredException(String message) {
        super(message);
    }

}
