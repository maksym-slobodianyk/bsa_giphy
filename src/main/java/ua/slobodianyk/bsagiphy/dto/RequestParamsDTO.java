package ua.slobodianyk.bsagiphy.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class RequestParamsDTO {
    private String query;
    private Boolean force;
}
