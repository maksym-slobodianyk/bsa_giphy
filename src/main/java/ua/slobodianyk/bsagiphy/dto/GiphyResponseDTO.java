package ua.slobodianyk.bsagiphy.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ua.slobodianyk.bsagiphy.utils.GifDeserializer;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@JsonDeserialize(using = GifDeserializer.class)
public class GiphyResponseDTO {
    private String id;
    private String url;
}
