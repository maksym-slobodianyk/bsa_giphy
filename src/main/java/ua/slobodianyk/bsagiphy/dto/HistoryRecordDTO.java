package ua.slobodianyk.bsagiphy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HistoryRecordDTO {
    String date;
    String query;
    String gif;
}
