package ua.slobodianyk.bsagiphy.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import ua.slobodianyk.bsagiphy.entity.GifsQueryFolder;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class CachedGifsRepository {
    @Autowired
    private static Random random;

    @Value("${file-storage.path}")
    String storagePath;

    public Optional<File> getGifByQuery(String query) {

        var queryDirectory = new File(storagePath + "/cache/" + query);

        if (queryDirectory.exists()) {

            var gifs = Arrays.stream(queryDirectory.listFiles())
                    .filter(f -> !f.isDirectory())
                    .collect(Collectors.toList());

            var randomIndex = random.nextInt(gifs.size());

            return Optional.of(gifs.get(randomIndex));
        }

        return Optional.empty();
    }

    public List<GifsQueryFolder> getCachedGifs(String query) {

        query = query != null ? query : "";
        var directory = new File(storagePath + "/cache/" + query);

        if (!directory.exists())
            return new ArrayList<>();

        if (query.equals(""))
            return Arrays.stream(Objects.requireNonNull(directory.listFiles()))
                    .filter(File::isDirectory)
                    .map(this::getGifsQueryFolder)
                    .collect(Collectors.toList());
        else
            return Arrays.asList(getGifsQueryFolder(directory));


    }

    public List<String> getAllGifs() {

        List<String> gifs = new ArrayList<>();

        var directory = new File(storagePath + "/cache");

        if (!directory.exists())
            return new ArrayList<>();

        Arrays.stream(Objects.requireNonNull(directory.listFiles()))
                .filter(File::isDirectory)
                .forEach(f -> gifs.addAll(getGifsQueryFolder(f).getGifs()));

        return gifs;
    }

    private GifsQueryFolder getGifsQueryFolder(File dir) {

        GifsQueryFolder folder = new GifsQueryFolder(dir.getName());

        Arrays.stream(Objects.requireNonNull(dir.listFiles()))
                .filter(f -> !f.isDirectory())
                .forEach(f -> folder.getGifs().add(f.getAbsolutePath()));

        return folder;
    }

}
