package ua.slobodianyk.bsagiphy.repository;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import ua.slobodianyk.bsagiphy.entity.GifsQueryFolder;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class UsersGifsRepository {


    @Value("${file-storage.path}")
    String storagePath;

    public List<GifsQueryFolder> getUsersGifs(String userId) {

        var usersDirectory = new File(storagePath + "/users/" + userId);

        if (!usersDirectory.exists())
            return new ArrayList<>();

        return Arrays.stream(usersDirectory.listFiles())
                .filter(File::isDirectory)
                .map(this::getGifsQueryFolder)
                .collect(Collectors.toList());
    }

    public Optional<String> search(String userId, String query) {
        Random random = new Random();
        var queryDirectory = new File(storagePath + "/users/" + userId + "/" + query);

        if (queryDirectory.exists()) {

            int randomIndex = random.nextInt(queryDirectory.listFiles().length);
            return Optional.of(queryDirectory.listFiles()[randomIndex].getAbsolutePath());

        } else {
            return Optional.empty();
        }
    }

    private GifsQueryFolder getGifsQueryFolder(File dir) {

        GifsQueryFolder folder = new GifsQueryFolder(dir.getName());

        Arrays.stream(dir.listFiles())
                .filter(f -> !f.isDirectory())
                .forEach(f -> folder.getGifs().add(f.getAbsolutePath()));

        return folder;
    }

}
