package ua.slobodianyk.bsagiphy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource({ "classpath:application-${envTarget:dev}.properties" })
public class BsaGiphyApplication {

    public static void main(String[] args) {
        SpringApplication.run(BsaGiphyApplication.class, args);
    }

}
