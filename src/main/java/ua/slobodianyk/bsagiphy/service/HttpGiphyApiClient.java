package ua.slobodianyk.bsagiphy.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Optional;

import org.apache.http.client.utils.URIBuilder;
import ua.slobodianyk.bsagiphy.dto.GiphyResponseDTO;

@Component
public final class HttpGiphyApiClient {

    private final HttpClient client;

    @Value("${api.giphy-endpoint}")
    private String giphyApiUrl;

    @Value("${api.giphy-api-key}")
    private String apiKey;

    @Autowired
    public HttpGiphyApiClient(HttpClient client) {

        this.client = client;
    }

    public Optional<GiphyResponseDTO> generateGifByQuery(String query) {
        try {

            var response = client.send(buildGetRequest(query), HttpResponse.BodyHandlers.ofString());

            GiphyResponseDTO generatedGif = new ObjectMapper().readValue(response.body(), GiphyResponseDTO.class);
            return Optional.of(generatedGif);

        } catch (IOException | InterruptedException ex) {
            ex.printStackTrace();
            return Optional.empty();
        }
    }

    private HttpRequest buildGetRequest(String query) {

        URI uri = null;
        try {
            uri = new URIBuilder(giphyApiUrl)
                    .addParameter("api_key", apiKey)
                    .addParameter("tag", query).build();
        } catch (URISyntaxException e) {
            //TODO: add loging
            e.printStackTrace();
        }

        return HttpRequest
                .newBuilder()
                .uri(uri)
                .GET()
                .build();
    }
}
