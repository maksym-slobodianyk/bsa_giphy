package ua.slobodianyk.bsagiphy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.slobodianyk.bsagiphy.dto.GiphyResponseDTO;
import ua.slobodianyk.bsagiphy.entity.GifsQueryFolder;
import ua.slobodianyk.bsagiphy.exceptions.GifNotFoundException;
import ua.slobodianyk.bsagiphy.repository.CachedGifsRepository;
import ua.slobodianyk.bsagiphy.repository.UsersGifsRepository;
import ua.slobodianyk.bsagiphy.utils.FileSystemUtil;
import ua.slobodianyk.bsagiphy.utils.UserIdValidator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Service
public class GifService {

    @Value("${file-storage.path}")
    String storagePath;

    private final CachedGifsRepository cachedGifsRepository;
    private final UsersGifsRepository usersGifsRepository;
    private final FileSystemUtil fileSystemUtil;
    private final HttpGiphyApiClient giphyApiClient;
    private final UserService historyService;
    private final UserIdValidator userIdValidator;
    private final MemoryCacheService memoryCacheService;

    @Autowired
    public GifService(CachedGifsRepository cachedGifsRepository,
                      FileSystemUtil fileSystemUtil,
                      HttpGiphyApiClient giphyApiClient,
                      UserService historyService,
                      UserIdValidator userIdValidator,
                      MemoryCacheService memoryCacheService,
                      UsersGifsRepository usersGifsRepository) {
        this.cachedGifsRepository = cachedGifsRepository;
        this.fileSystemUtil = fileSystemUtil;
        this.giphyApiClient = giphyApiClient;
        this.historyService = historyService;
        this.userIdValidator = userIdValidator;
        this.memoryCacheService = memoryCacheService;
        this.usersGifsRepository = usersGifsRepository;
    }

    public String generateGif(String userId, String query, Boolean force) throws IOException {

        userIdValidator.validate(userId);

        var generatedGifPath = "users/" + userId + "/" + query + "/";

        Optional<File> cachedGif = Optional.empty();

        if (!force)
            cachedGif = cachedGifsRepository.getGifByQuery(query);

        if (!force && cachedGif.isPresent()) {

            fileSystemUtil.copyGif(cachedGif.get().getAbsolutePath(), generatedGifPath, cachedGif.get().getName());
            generatedGifPath += cachedGif.get().getName();

        } else {
            Optional<GiphyResponseDTO> generatedGif;

            do {
                generatedGif = giphyApiClient.generateGifByQuery(query);
                if (generatedGif.isEmpty()) throw new IOException();
            } while (Files.exists(Paths.get(storagePath + generatedGifPath + generatedGif.get().getId() + ".gif"))); //Query Giphy until we get unique gif

            fileSystemUtil.saveGifByUrl(generatedGif.get().getUrl(), generatedGifPath, generatedGif.get().getId() + ".gif"); //Save to users folder
            fileSystemUtil.saveGifByUrl(generatedGif.get().getUrl(), "cache/" + query + "/", generatedGif.get().getId() + ".gif"); //Save to disk cache

            generatedGifPath += generatedGif.get().getId() + ".gif";

        }
        historyService.addHistoryRecord(userId, query, generatedGifPath);
        memoryCacheService.addRecord(userId, query, generatedGifPath);

        return storagePath + generatedGifPath;
    }

    public GifsQueryFolder generateGif(String query) throws IOException {
        Optional<GiphyResponseDTO> generatedGif;

        do {
            generatedGif = giphyApiClient.generateGifByQuery(query);
            if (generatedGif.isEmpty()) throw new IOException();
        } while (Files.exists(Paths.get(storagePath + "/cache/" + query + "/" + generatedGif.get().getId() + ".gif"))); //Query Giphy until we get unique gif

        fileSystemUtil.saveGifByUrl(generatedGif.get().getUrl(), "cache/" + query + "/", generatedGif.get().getId() + ".gif");

        return cachedGifsRepository.getCachedGifs(query).get(0);
    }

    public List<GifsQueryFolder> getUsersGifs(String id) {

        return usersGifsRepository.getUsersGifs(id);
    }

    public List<GifsQueryFolder> getCachedGifs(String query) {

        return cachedGifsRepository.getCachedGifs(query);

    }

    public List<String> getAllGifs() {

        return cachedGifsRepository.getAllGifs();
    }

    public String search(String userId, String query, Boolean force) {

        if (!force) {

            Optional<String> result = memoryCacheService.search(userId, query);
            if (result.isPresent())
                return result.get();

        }

        Optional<String> result = usersGifsRepository.search(userId, query);

        if (result.isPresent()) {
            memoryCacheService.addRecord(userId, query, result.get());
            return result.get();
        } else {
            throw new GifNotFoundException("No GIF was found by query: " + query);
        }

    }

    public void deleteCachedGifs() {
        fileSystemUtil.deleteFolder("/cache");
    }

}
