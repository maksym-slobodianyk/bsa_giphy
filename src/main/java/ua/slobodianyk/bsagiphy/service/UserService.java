package ua.slobodianyk.bsagiphy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ua.slobodianyk.bsagiphy.dto.HistoryRecordDTO;
import ua.slobodianyk.bsagiphy.utils.FileSystemUtil;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class UserService {


    private final FileSystemUtil fileSystemUtil;


    @Autowired
    public UserService(FileSystemUtil fileSystemUtil) {
        this.fileSystemUtil = fileSystemUtil;
    }

    @Value("${file-storage.path}")
    String storagePath;

    public void addHistoryRecord(String userId, String query, String generatedGifPath) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy, ");
        String record = formatter.format(new Date()) + query + ", " + storagePath + "/" + generatedGifPath + "\n";

        try {
            FileWriter writer = new FileWriter(storagePath + "users/" + userId + "/history.csv", true);
            writer.write(record);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clearHistory(String userId) throws IOException {
        try {
            FileWriter writer = new FileWriter(storagePath + "users/" + userId + "/history.csv");
            writer.write("");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public List<HistoryRecordDTO> getAllHistoryRecords(String userId) throws IOException {

        if (!Files.exists(Paths.get(storagePath + "users/" + userId + "/history.csv")))
            return new ArrayList<>();

        List<HistoryRecordDTO> records = new ArrayList<>();

        try (FileReader fr = new FileReader(new File(storagePath + "users/" + userId + "/history.csv"));

             BufferedReader br = new BufferedReader(fr)) {

            String line;
            while ((line = br.readLine()) != null) {

                String[] values = line.split(", ");
                records.add(new HistoryRecordDTO(values[0], values[1], values[2]));

            }
            return records;

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void deleteUserData(String userId) {

        fileSystemUtil.deleteFolder("/users/" + userId);
    }
}
