package ua.slobodianyk.bsagiphy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.slobodianyk.bsagiphy.entity.GifsQueryFolder;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class MemoryCacheService {

    @Autowired
    private static Random random;

    private final Map<String, List<GifsQueryFolder>> memoryCache = new HashMap<>();

    public void addRecord(String userId, String query, String generatedGifPath) {

        List<GifsQueryFolder> folders = memoryCache.computeIfAbsent(userId, k -> new ArrayList<>());

        GifsQueryFolder queryFolder = folders.stream()
                .filter(a -> a.getQuery().equals(query))
                .findFirst()
                .orElse(new GifsQueryFolder(query));

        if (queryFolder.getGifs().size() == 0)
            folders.add(queryFolder);

        queryFolder.getGifs().add(generatedGifPath);
    }

    public void clearUserCache(String userId) {

        memoryCache.remove(userId);

    }

    public void clearUsersQueryCache(String userId, String query) {

        List<GifsQueryFolder> folders = memoryCache.get(userId);

        if (folders != null) {
            folders = folders.stream().filter(f -> !f.getQuery().equals(query)).collect(Collectors.toList());
        }

        memoryCache.put(userId, folders);
    }

    public Optional<String> search(String userId, String query) {

        List<GifsQueryFolder> folders = memoryCache.get(userId);

        if (folders != null) {

            Optional<GifsQueryFolder> queryFolder = folders.stream()
                    .filter(a -> a.getQuery().equals(query))
                    .findFirst();

            if (queryFolder.isPresent()) {
                var randomIndex = random.nextInt(queryFolder.get().getGifs().size());
                return Optional.of(queryFolder.get().getGifs().get(randomIndex));
            }

        }
        return Optional.empty();
    }

}
