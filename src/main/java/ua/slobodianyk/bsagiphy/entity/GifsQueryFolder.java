package ua.slobodianyk.bsagiphy.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class GifsQueryFolder {
    String query;
    List<String> gifs;

    public GifsQueryFolder(String query) {
        this.query = query;
        gifs = new ArrayList<>();
    }
}
