package ua.slobodianyk.bsagiphy.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ua.slobodianyk.bsagiphy.dto.RequestParamsDTO;
import ua.slobodianyk.bsagiphy.entity.GifsQueryFolder;
import ua.slobodianyk.bsagiphy.service.GifService;

import java.io.IOException;
import java.util.List;

@RestController
public class ApiCacheController {

    private final GifService gifService;

    @Autowired
    public ApiCacheController(GifService gifService) {
        this.gifService = gifService;
    }

    @ApiOperation(value = "Get cache from disk",
            notes = "Provide query to select all GIFs from specific folder",
            response = GifsQueryFolder.class,
            responseContainer = "List")
    @GetMapping("/cache")
    public List<GifsQueryFolder> getCachedGifs(@ApiParam(value = "query folder to select from", required = false)
                                               @RequestParam(required = false) String query) {

        return gifService.getCachedGifs(query);
    }

    @ApiOperation(value = "Get all GIFs from cache",
            response = String.class,
            responseContainer = "List")
    @GetMapping("/gifs")
    public List<String> getAllGifs() {

        return gifService.getAllGifs();
    }

    @ApiOperation(value = "Download GIF from GIPHY and save it to cache",
            response = GifsQueryFolder.class)
    @PostMapping("/cache/generate")
    public GifsQueryFolder generateGif(@ApiParam(value = "query to search for in giphy, force is unimportant", required = true)
                                       @RequestBody RequestParamsDTO params) throws IOException {

        return gifService.generateGif(params.getQuery());
    }

    @ApiOperation(value = "Delete all GIFs from cache")
    @DeleteMapping("/cache")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCachedGifs() {

        gifService.deleteCachedGifs();
    }

}
