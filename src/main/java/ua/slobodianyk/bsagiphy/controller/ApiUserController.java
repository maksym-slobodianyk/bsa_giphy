package ua.slobodianyk.bsagiphy.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ua.slobodianyk.bsagiphy.dto.RequestParamsDTO;
import ua.slobodianyk.bsagiphy.dto.HistoryRecordDTO;
import ua.slobodianyk.bsagiphy.entity.GifsQueryFolder;
import ua.slobodianyk.bsagiphy.service.MemoryCacheService;
import ua.slobodianyk.bsagiphy.service.GifService;
import ua.slobodianyk.bsagiphy.service.UserService;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/user")
public class ApiUserController {

    private final UserService userService;
    private final GifService gifService;
    private final MemoryCacheService memoryCacheService;

    @Autowired
    public ApiUserController(UserService userService, GifService gifService, MemoryCacheService memoryCacheService) {
        this.userService = userService;
        this.gifService = gifService;
        this.memoryCacheService = memoryCacheService;
    }


    @ApiOperation(value = "Get user history",
            response = HistoryRecordDTO.class,
            responseContainer = "List")
    @GetMapping("/{id}/history")
    public List<HistoryRecordDTO> getUserHistory(@ApiParam(value = "User id", defaultValue = "Jack", required = true)
                                                 @PathVariable String id) throws IOException {

        return userService.getAllHistoryRecords(id);
    }


    @ApiOperation(value = "Get all files from user folder",
            response = GifsQueryFolder.class,
            responseContainer = "List")
    @GetMapping("/{id}/all")
    public List<GifsQueryFolder> getUserCache(@ApiParam(value = "User id", defaultValue = "Jack", required = true)
                                              @PathVariable String id) {

        return gifService.getUsersGifs(id);
    }


    @ApiOperation(value = "Search for a GIF in user cache and folder (DOESN'T WORK IN SWAGGER, BUT DOES IN POSTMAN)",
            response = String.class)
    @GetMapping("/{id}/search")
    public String searchGif(@ApiParam(value = "User id", defaultValue = "Jack", required = true)
                            @PathVariable String id,
                            @ApiParam(value = "query - query to search for\nset force param to true to skip searching in cache", required = true)
                            @RequestBody RequestParamsDTO params) {

        return gifService.search(id, params.getQuery(), params.getForce());
    }


    @ApiOperation(value = "Generate GIF by query in user folder",
            response = String.class)
    @PostMapping("/{id}/generate")
    public String generateGif(@ApiParam(value = "User id", defaultValue = "Jack", required = true)
                              @PathVariable String id,
                              @ApiParam(value = "query - query to generate with\nforce is unimportant", required = true)
                              @RequestBody RequestParamsDTO params) throws IOException {

        return gifService.generateGif(id, params.getQuery(), params.getForce());
    }


    @ApiOperation(value = "Clean up user history")
    @DeleteMapping("/{id}/history/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserHistory(@ApiParam(value = "User id", defaultValue = "Jack", required = true)
                                  @PathVariable String id) throws IOException {

        userService.clearHistory(id);
    }

    @ApiOperation(value = "Clean up users memory cache",
            notes = "Provide query to clean only specific folder")
    @DeleteMapping("/{id}/reset")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserCache(@ApiParam(value = "User id", defaultValue = "Jack", required = true)
                                @PathVariable String id,
                                @ApiParam(value = "folder to clean up", defaultValue = "apple", required = false)
                                @RequestParam(required = false) String query) {
        if (query != null)
            memoryCacheService.clearUsersQueryCache(id, query);
        else
            memoryCacheService.clearUserCache(id);
    }

    @ApiOperation(value = "Delete all user data")
    @DeleteMapping("/{id}/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserData(@ApiParam(value = "User id", defaultValue = "Jack", required = true)
                               @PathVariable String id) {
        memoryCacheService.clearUserCache(id);
        userService.deleteUserData(id);
    }
}
