package ua.slobodianyk.bsagiphy.interceptors;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ua.slobodianyk.bsagiphy.exceptions.HeaderRequiredException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class RequestHeaderInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        if (request.getHeader("X-BSA-GIPHY") == null)
            throw new HeaderRequiredException();
        return true;
    }
}
