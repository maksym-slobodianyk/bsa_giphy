package ua.slobodianyk.bsagiphy.utils;

import org.springframework.stereotype.Component;

@Component
public class UserIdValidator {
    public void validate(String userId) {
        if (!userId.matches("[^\\\\/:*?”<>|]+"))
            throw new IllegalArgumentException("Bad user id");
    }
}
