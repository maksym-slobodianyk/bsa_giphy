package ua.slobodianyk.bsagiphy.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;

@Component
public class FileSystemUtil {

    @Value("${file-storage.path}")
    String storagePath;

    public void copyGif(String sourcePath, String destinationPath, String fileName) throws IOException {

        destinationPath = storagePath + destinationPath;

        if (!Files.exists(Paths.get(destinationPath)))
            new File(destinationPath).mkdirs();

        try (var fis = new FileInputStream(sourcePath);
             var fos = new FileOutputStream(new File(destinationPath + fileName))) {

            byte[] buffer = new byte[1024];
            int length;

            while ((length = fis.read(buffer)) > 0)
                fos.write(buffer, 0, length);

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void saveGifByUrl(String sourceURL, String destinationPath, String fileName) throws IOException {

        destinationPath = storagePath + destinationPath;

        if (!Files.exists(Paths.get(destinationPath)))
            new File(destinationPath).mkdirs();

        if (!Files.exists(Paths.get(destinationPath + fileName)))

            try (var fis = new URL(sourceURL).openStream();
                 var fos = new FileOutputStream(new File(destinationPath + fileName))) {

                byte[] buffer = new byte[1024];
                int length;

                while ((length = fis.read(buffer)) > 0)
                    fos.write(buffer, 0, length);

            } catch (IOException e) {
                e.printStackTrace();
                throw e;
            }
    }

    public void deleteFolder(String path) {
        var directory = new File(storagePath + path);

        if (directory.exists()) {

            Arrays.stream(Objects.requireNonNull(directory.listFiles()))
                    .forEach(f -> {
                        if (f.isDirectory())
                            Arrays.stream(Objects.requireNonNull(f.listFiles()))
                                    .forEach(File::delete);
                        f.delete();
                    });

        }
        directory.delete();
    }
}
