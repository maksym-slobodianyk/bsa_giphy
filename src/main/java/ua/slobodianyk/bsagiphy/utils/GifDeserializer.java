package ua.slobodianyk.bsagiphy.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import ua.slobodianyk.bsagiphy.dto.GiphyResponseDTO;

import java.io.IOException;

public class GifDeserializer extends StdDeserializer<GiphyResponseDTO> {

    public GifDeserializer() {
        this(null);
    }

    public GifDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public GiphyResponseDTO deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

        JsonNode node = jp.getCodec().readTree(jp);

        String id = node.get("data").get("id").asText();
        String url = node.get("data").get("images").get("downsized_large").get("url").asText();
        url = url.replaceAll("(media\\d+\\.giphy\\.com)", "i.giphy.com");

        return new GiphyResponseDTO(id, url);
    }
}